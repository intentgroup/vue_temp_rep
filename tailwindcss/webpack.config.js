const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require("path");
const NodePolyfillPlugin = require("node-polyfill-webpack-plugin")
const sveltePreprocess = require('svelte-preprocess');

const mode = process.env.NODE_ENV || "development";
const prod = mode === "production";

module.exports = {
  entry: {
    "build/bundle": ["./src/main.js"],
  },
  resolve: {
    alias: {
      svelte: path.dirname(require.resolve("svelte/package.json")),
    },
    extensions: [".mjs", ".js", ".svelte"],
    mainFields: ["svelte", "browser", "module", "main"],
  },
  output: {
    path: path.join(__dirname, "../dist/admin"),
    filename: "[name].js",
    chunkFilename: "[name].[id].js",
  },
  module: {
    rules: [
      {
        test: /\.(html|svelte)$/,
        use: {
          loader: "svelte-loader",
          options: {
            compilerOptions: {
              dev: !prod,
            },
            preprocess: sveltePreprocess({
              babel: {
                presets: [
                  [
                    '@babel/preset-env',
                    {
                      loose: true,
                      // No need for babel to resolve modules
                      modules: false,
                      targets: {
                        // ! Very important. Target es6+
                        esmodules: true,
                      },
                    },
                  ],
                ],
              },
              postcss: {
                sourceMap: !prod,
                plugins: [
                  require("tailwindcss"),
                  require("autoprefixer"),
                  require("postcss-nesting")
                ],
              },
            }),
            emitCss: prod,
            hotReload: !prod,
          },
        },
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
      {
        test: /\.(?:ico|gif|png|jpg|jpeg|webp|svg)$/i,
        use: [
          {
            loader: "file-loader",
            options: {
              name: `[name].[ext]`
            }
          }
        ]
      },
      {
        // required to prevent errors from Svelte on Webpack 5+
        test: /node_modules\/svelte\/.*\.mjs$/,
        resolve: {
          fullySpecified: false,
        },
      },
    ],
  },
  mode,
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name].css",
    }),
    new NodePolyfillPlugin()
  ],
  devtool: prod ? false : "source-map",
  devServer: {
    hot: true,
  },
};