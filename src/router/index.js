import Vue from "vue";
import Router from "vue-router";

import Auth from "../components/Auth/Auth";
import Account from "../components/Account/Account";
import { AmplifyEventBus } from "aws-amplify-vue";
import { getUser } from "@/utils/auth.js";

Vue.use(Router);

const router = new Router({
  linkExactActiveClass: "active",
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    { path: "/sign-up.html", redirect: "/app.html" },
    {
      path: "/app.html",
      component: Auth,
      children: [
        {
          path: "",
          name: "Зарегистрироваться",
          component: () => import("../components/Auth/SingUp/SingUp")
        },
        {
          path: "enter",
          name: "Войти",
          component: () => import("../components/Auth/SingIn/SingIn")
        },
        {
          path: "reset-password",
          name: "Сброс пароля",
          component: () =>
            import("../components/Auth/ResetPassword/ResetPassword")
        }
      ]
    },
    {
      path: "/app.html/account",
      component: Account,
      meta: { requiresAuth: true },
      children: [
        {
          path: "personal",
          name: "Личный кабинет",
          component: () => import("../components/Account/Account_personal")
        },
        {
          path: "logs",
          name: "Журнал событий",
          component: () => import("../components/Account/Account_logs")
        },
        {
          path: "",
          name: "Роутер",
          component: () =>
            import("../components/Account/Account_router/Account_router")
        },
        {
          path: "help",
          name: "Помощь",
          component: () =>
            import("../components/Account/Account_help/Account_help")
        },
        {
          path: "settings",
          name: "Настройки",
          component: () =>
            import("../components/Account/Account_settings/Account_settings")
        },
        {
          path: "program",
          name: "Партнерская программа",
          component: () =>
            import("../components/Account/Account_program/Account_program")
        }
      ]
    }
  ]
});

AmplifyEventBus.$on("authState", async state => {
  const pushPathes = {
    signedOut: () => {
      router.push({ path: "/app.html" });
    },
    signedIn: () => {
      router.push({ path: "/app.html/account" });
    }
  };
  if (typeof pushPathes[state] === "function") {
    pushPathes[state]();
  }
});

router.beforeResolve(async (to, from, next) => {
  const user = await getUser();
  if (!user) {
    if (to.matched.some(record => record.meta.requiresAuth)) {
      return next({
        path: "/app.html"
      });
    }
  } else {
    if (
      to.matched.some(
        record =>
          typeof record.meta.requiresAuth === "boolean" &&
          !record.meta.requiresAuth
      )
    ) {
      return next({
        path: "/"
      });
    }
  }
  return next();
});
export default router;
