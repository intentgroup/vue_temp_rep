import { API, graphqlOperation } from "aws-amplify";
import * as queries from "../../graphql/queries";
import * as mutations from "../../graphql/mutations";

const EventList = {
  namespaced: true,
  state: () => ({
    events: null
  }),
  getters: {
    newEventsLength: state => {
      if (state.events !== null) {
        let newLength = state.events.customerEvents.filter(
          el => el.isRead === false
        );
        return newLength.length;
      }
    },
    eventsLength: state => state.events.customerEvents.length
  },
  mutations: {
    setNewPageEventList(state, { type, items: { customerEvents, id } }) {
      state[type].customerEvents = [
        ...state[type].customerEvents,
        ...customerEvents
      ];
      state[type].id = id;
    },
    setEventAsRead(state, { type, items: { id }, items }) {
      state[type].customerEvents = [
        ...state[type].customerEvents.filter(item => item.id !== id),
        items
      ];
    }
  },
  actions: {
    async getCustomerEventsPage({ commit, dispatch, state }, previousPageId) {
      try {
        const {
          data: { getCustomerEventsPage }
        } = await API.graphql(
          graphqlOperation(queries.getCustomerEventsPage, { previousPageId })
        );

        if (state.events === null) {
          commit(
            "setData",
            {
              module: "EventsList",
              type: "events",
              items: getCustomerEventsPage
            },
            { root: true }
          );

          getCustomerEventsPage.customerEvents.forEach(item => {
            item.pageId = getCustomerEventsPage.id;
          });

          return { nextId: getCustomerEventsPage.id };
        } else {
          commit("setNewPageEventList", {
            type: "events",
            items: getCustomerEventsPage
          });

          getCustomerEventsPage.customerEvents.forEach(item => {
            item.pageId = getCustomerEventsPage.id;
          });

          return { nextId: getCustomerEventsPage.id };
        }
      } catch (err) {
        dispatch("setError", err.message, { root: true });
        throw new Error(err);
      }
    },
    async trackCustomerEvent({ commit, dispatch }, message) {
      try {
        const {
          data: { trackCustomerEvent }
        } = await API.graphql(
          graphqlOperation(mutations.trackCustomerEvent, { message })
        );
        commit(
          "setData",
          {
            module: "EventsList",
            type: "trackCustomerEvent",
            items: trackCustomerEvent
          },
          { root: true }
        );
      } catch (err) {
        dispatch("setError", err.message, { root: true });
        throw new Error(err);
      }
    },

    async setEventsAsRead({ commit }, payload) {
      let { id, pageId } = payload;
      const {
        data: {
          markCustomerEventsRead: { data: item }
        }
      } = await API.graphql({
        query: mutations.markCustomerEventsRead,
        variables: { pageId: pageId, messageIds: [id] }
      });

      /**
       * @Kludge: neither robust nor clean, but gets the job done
       */
      commit("setEventAsRead", {
        type: "events",
        items: item[0]
      });
    }
  }
};
export default EventList;
