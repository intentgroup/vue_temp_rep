// import { API, graphqlOperation } from "aws-amplify";
import * as queries from "../../graphql/queries";
import * as mutations from "../../graphql/mutations";
import { API, graphqlOperation } from "aws-amplify";
import { bus } from "@/main";

const Settings = {
  namespaced: true,
  state: () => ({
    serverLocations: null,
    connectionTypes: null,
    currentServerLocation: null,
    currentServerLocationAddress: null
  }),
  getters: {
    currentServerLocationAddress: state =>
      state.serverLocations
        .filter(address => address.title === state.currentServerLocation)
        .map(address => address.address)
  },
  mutations: {},
  actions: {
    async getConnectionTypes({ dispatch, commit }) {
      try {
        const {
          data: { connectionTypes: items }
        } = await API.graphql(graphqlOperation(queries.connectionTypes));
        commit(
          "setData",
          {
            module: "Settings",
            type: "connectionTypes",
            items
          },
          { root: true }
        );
      } catch (err) {
        dispatch("setError", err.errors[0].message, { root: true });
        throw new Error(err);
      }
    },

    getCurrentSelectedServerLocation({ commit }, { title: items }) {
      commit(
        "setData",
        {
          module: "Settings",
          type: "currentServerLocation",
          items
        },
        { root: true }
      );
    },

    async updateConnectionType({ dispatch }, { newConnectionTypeTitle }) {
      try {
        await API.graphql(
          graphqlOperation(mutations.updateConnectionType, {
            newConnectionTypeTitle
          })
        );
      } catch (err) {
        /**
         * @Kludge: neither robust nor clean, but gets the job done
         */
        dispatch("setError", err.errors[0].message, { root: true });
        throw new Error(err);
      }
    },

    async getServerLocations({ dispatch, commit }) {
      try {
        const {
          data: { serverLocations: items }
        } = await API.graphql(graphqlOperation(queries.serverLocations));
        commit(
          "setData",
          {
            module: "Settings",
            type: "serverLocations",
            items
          },
          { root: true }
        );
      } catch (err) {
        /**
         * @Kludge: neither robust nor clean, but gets the job done
         */
        dispatch("setError", err.errors[0].message, { root: true });
        throw new Error(err);
      }
    },
    async updateServerLocation({ dispatch }, { newServerLocationId }) {
      try {
        await API.graphql(
          graphqlOperation(mutations.updateServerLocation, {
            newServerLocationId
          })
        );
      } catch (err) {
        /**
         * @Kludge: neither robust nor clean, but gets the job done
         */
        dispatch("setError", err.errors[0].message, { root: true });
        throw new Error(err);
      }
    },
    async connectRouter({ dispatch }, { routerId }) {
      try {
        const {
          data: { connectRouter }
        } = await API.graphql(
          graphqlOperation(mutations.connectRouter, {
            routerId
          })
        );

        console.group("connectRouter response");
        console.log(
          "%c connectRouter ",
          "color: white; background-color: #2274A5"
        );
        console.table("connectRouter: ", connectRouter);
        console.groupEnd();
      } catch (err) {
        /**
         * @Kludge: neither robust nor clean, but gets the job done
         */
        bus.$emit("toggle-router", {
          toggle: false,
          message: ""
        });
        dispatch("setError", err.errors[0].message, { root: true });
        throw new Error(err);
      }
    },
    async disconnectRouter({ dispatch }, { routerId }) {
      try {
        const {
          data: { disconnectRouter }
        } = await API.graphql(
          graphqlOperation(mutations.disconnectRouter, {
            routerId
          })
        );
        console.group("disconnectRouter response");
        console.log(
          "%c disconnectRouter ",
          "color: white; background-color: #2274A5"
        );
        console.table("disconnectRouter: ", disconnectRouter);
        console.groupEnd();
      } catch (err) {
        /**
         * @Kludge: neither robust nor clean, but gets the job done
         */
        bus.$emit("toggle-router", {
          toggle: false,
          message: ""
        });
        dispatch("setError", err.errors[0].message, { root: true });
        throw new Error(err);
      }
    },
    async createVpnBypassMacAddress({ dispatch }, payload) {
      try {
        await API.graphql(
          graphqlOperation(mutations.createVpnBypassMacAddress, {
            address: payload
          })
        );
      } catch (err) {
        /**
         * @Kludge: neither robust nor clean, but gets the job done
         */
        dispatch("setError", err.errors[0].message, { root: true });
        throw new Error(err);
      }
    },
    async removeVpnBypassMacAddress({ dispatch }, payload) {
      try {
        await API.graphql(
          graphqlOperation(mutations.removeVpnBypassMacAddress, {
            address: payload
          })
        );
      } catch (err) {
        /**
         * @Kludge: neither robust nor clean, but gets the job done
         */
        dispatch("setError", err.errors[0].message, { root: true });
        throw new Error(err);
      }
    },
    async updateVpnBypassMacAddressesAllowed({ dispatch }, { newValue }) {
      try {
        await API.graphql(
          graphqlOperation(mutations.updateVpnBypassMacAddressesAllowed, {
            newValue
          })
        );
      } catch (err) {
        dispatch("setError", err.message, { root: true });
        throw new Error(err);
      }
    }
  }
};
export default Settings;
