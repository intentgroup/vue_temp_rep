import Vue from "vue";
import Vuex from "vuex";

import User from "./User";
import EventsList from "./EventsList";
import Router from "./Router";
import Settings from "./Settings";

Vue.use(Vuex);

export default new Vuex.Store({
  strict: true,
  state: {
    reqError: null,
    currPage: null
  },
  mutations: {
    setError(state, { type, items }) {
      state[type] = items;
    },
    setData(state, { module, type, items }) {
      state[module][type] = items;
    },
    setLocalData(state, { type, items }) {
      state[type] = items;
    },
    unsetModuleData(state, { module }) {
      for (let key in state[module]) {
        state[module][key] = null;
      }
    }
  },
  actions: {
    setError({ commit }, payload) {
      commit("setError", { type: "reqError", items: payload });
    },
    currentPage({ commit }, payload) {
      commit("setLocalData", { type: "currPage", items: payload });
    },
    removeData({ commit }, payload) {
      commit("unsetModuleData", { module: payload });
    }
  },
  modules: {
    User,
    EventsList,
    Router,
    Settings
  }
});
