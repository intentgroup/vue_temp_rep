import { API, graphqlOperation } from "aws-amplify";
import * as queries from "../../graphql/queries";

const User = {
  namespaced: true,
  state: () => ({
    user: null,
    registrationDate: null,
    newPassData: null,
    customerInfo: null
  }),
  getters: {
    getLetterOfName: state => state.user.name[0].toUpperCase()
  },
  mutations: {
    mutationMode(state, { type, items }) {
      state[type].connectionSettings.vpnBypassMACAddressesAllowed = items;
    },
    mutationServerLocation(state, { type, items }) {
      state[type].connectionSettings.serverLocation = items;
    },
    mutationRouterConnect(state, { type, items }) {
      state[type].connectionSettings.connected = items;
    },
    addedNewMac(state, { type, items }) {
      state[type].connectionSettings.vpnBypassMACAddresses = [
        ...state[type].connectionSettings.vpnBypassMACAddresses,
        items
      ];
    },
    removeMac(state, { type, items }) {
      state[type].connectionSettings.vpnBypassMACAddresses = state[
        type
      ].connectionSettings.vpnBypassMACAddresses.filter(mac => {
        return mac !== items;
      });
    }
  },
  actions: {
    async getCustomerInfo({ commit, dispatch }) {
      try {
        const {
          data: { customerInfo }
        } = await API.graphql(graphqlOperation(queries.customerInfo));
        commit(
          "setData",
          {
            module: "User",
            type: "customerInfo",
            items: customerInfo
          },
          { root: true }
        );
      } catch (err) {
        /**
         * @Kludge: neither robust nor clean, but gets the job done
         */
        dispatch("setError", err.errors[0].message, { root: true });
        throw new Error(err);
      }
    },
    setRegistrationDate({ commit }, payload) {
      commit(
        "setData",
        {
          module: "User",
          type: "registrationDate",
          items: payload
        },
        { root: true }
      );
    },
    setNewPasswordData({ commit }, payload) {
      commit(
        "setData",
        {
          module: "User",
          type: "newPassData",
          items: payload
        },
        { root: true }
      );
    },
    setUser({ commit }, payload) {
      const { username, attributes } = payload;
      commit(
        "setData",
        {
          module: "User",
          type: "user",
          items: { username, ...attributes }
        },
        { root: true }
      );
    },
    setInitUserDate({ commit }, payload) {
      const {
        username,
        attributes: { email, email_verified, "custom:user_type": type }
      } = payload;
      commit(
        "setData",
        {
          module: "User",
          type: "user",
          items: { username, email, email_verified, type }
        },
        { root: true }
      );
    }
  }
};

export default User;
