import { API, graphqlOperation } from "aws-amplify";
import * as queries from "@/graphql/queries";
import * as mutations from "@/graphql/mutations";
import checkIp from "check-ip";
import { bus } from "@/main";
import { saveAs } from "file-saver";
import JSZip from "jszip";

const Router = {
  namespaced: true,
  state: () => ({
    customerRouters: null,
    updateRouter: null,
    lastRouterCommand: null,
    verifyData: null,
    manualConfiguration: null,
    manualManipulation: null
  }),
  getters: {
    getCurrentRouter: state => {
      if (state.customerRouters) {
        return {
          password: null,
          ...state.customerRouters
        };
      }
      return null;
    },
    getСheckIp: state => {
      const ip = checkIp(state.customerRouters.ipAddress);
      if (
        ip.isPublicIp &&
        ip.isValid &&
        !ip.isRfc1918 &&
        !ip.isMulticast &&
        !ip.isBogon &&
        !ip.isApipa
      ) {
        // white
        return true;
      } else {
        // gray
        return false;
      }
    }
  },
  mutations: {},
  actions: {
    async keysGenerate({ dispatch, state, rootState }, userFree) {
      await dispatch("getManualConfiguration");

      if (state.manualConfiguration !== null) {
        const { certificate, privateKey, script } = state.manualConfiguration;
        const zip = new JSZip();
        if (userFree) {
          zip.file(`${rootState.User.customerInfo.login}.crt`, certificate);
          zip.file(`${rootState.User.customerInfo.login}.key`, privateKey);
        } else {
          zip.file(`${rootState.User.customerInfo.login}.crt`, certificate);
          zip.file(`${rootState.User.customerInfo.login}.key`, privateKey);
          zip.file(`${rootState.Settings.currentServerLocation}.rsc`, script);
        }
        zip.generateAsync({ type: "blob" }).then(function (content) {
          saveAs(content, "manual-configuration.zip");
        });
      }
    },

    async getManualConfiguration({ commit, dispatch }) {
      try {
        const {
          data: { manualConfiguration: items }
        } = await API.graphql(graphqlOperation(queries.manualConfiguration));
        commit(
          "setData",
          {
            module: "Router",
            type: "manualConfiguration",
            items
          },
          { root: true }
        );
      } catch (err) {
        dispatch("setError", err.message, { root: true });
        throw new Error(err);
      }
    },
    /**
     * @description test functional
     * @param {newPlanId}
     */
    async changePlan({ dispatch, commit }, { newPlanId }) {
      try {
        const {
          data: {
            changePlan: { data: items }
          }
        } = await API.graphql({
          query: mutations.changePlan,
          variables: { newPlanId }
        });
        commit(
          "setData",
          {
            module: "User",
            type: "customerInfo",
            items
          },
          { root: true }
        );
      } catch (err) {
        dispatch("setError", err.errors[0].message, { root: true });
        throw new Error(err);
      }
    },

    async checkRouterCommandStatus({ dispatch }, { commandId }) {
      try {
        const {
          data: { checkRouterCommandStatus }
        } = await await API.graphql({
          query: queries.checkRouterCommandStatus,
          variables: { commandId }
        });
        console.group("54 line: index.js ");
        console.log(
          "%c checkRouterCommandStatus response",
          "color: white; background-color: #2274A5"
        );
        console.table("checkRouterCommandStatus: ", checkRouterCommandStatus);
        console.groupEnd();
      } catch (err) {
        dispatch("setError", err.errors[0].message, { root: true });
        throw new Error(err);
      }
    },

    async verifyRouter({ commit, dispatch }, { routerId }) {
      try {
        const {
          data: { verifyRouter: items }
        } = await await API.graphql({
          query: mutations.verifyRouter,
          variables: { routerId }
        });
        commit(
          "setData",
          {
            module: "Router",
            type: "verifyData",
            items
          },
          { root: true }
        );
        bus.$emit("router-verify");
      } catch (err) {
        dispatch("setError", err.errors[0].message, { root: true });
        throw new Error(err);
      }
    },

    async getRouterData({ commit, dispatch }) {
      try {
        const {
          data: { customerRouters: items }
        } = await API.graphql(graphqlOperation(queries.customerRouters));
        commit(
          "setData",
          {
            module: "Router",
            type: "customerRouters",
            items: items[0]
          },
          { root: true }
        );
        console.group("getRouterData");
        console.log(
          "%c Router Data ",
          "color: white; background-color: #2274A5"
        );
        console.table("Data: ", items[0]);
        console.groupEnd();
      } catch (err) {
        dispatch("setError", err.message, { root: true });
        throw new Error(err);
      }
    },

    async updateConnect({ dispatch }, payload) {
      try {
        await API.graphql({
          query: mutations.updateRouter,
          variables: { routerInfo: { ...payload } }
        });
      } catch (err) {
        dispatch("setError", err.errors[0].message, { root: true });
        throw new Error(err);
      }
    },

    async applyRouterSettings({ commit, dispatch }, { routerId }) {
      try {
        const {
          data: {
            applyRouterSettings: { data: items }
          }
        } = await API.graphql({
          query: mutations.applyRouterSettings,
          variables: { routerId }
        });
        commit(
          "setData",
          {
            module: "Router",
            type: "lastRouterCommand",
            items
          },
          { root: true }
        );

        return { items };
      } catch (err) {
        dispatch("setError", err.message, { root: true });
        throw new Error(err);
      }
    },

    async getManualManipulation({ commit, dispatch }) {
      try {
        const {
          data: { manualManipulation: items }
        } = await API.graphql({
          query: queries.manualManipulation
        });
        commit(
          "setData",
          {
            module: "Router",
            type: "manualManipulation",
            items: items
          },
          { root: true }
        );

        return { items };
      } catch (err) {
        dispatch("setError", err.message, { root: true });
        throw new Error(err);
      }
    }
  }
};
export default Router;
