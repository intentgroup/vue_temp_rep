import $ from "jquery";

export default () => {
  $(".main-security__trigger-item").on("mouseover", function hoverTrigger() {
    const number = $(this).data("number");
    $(".main-security__trigger-item").removeClass("active");
    $(this).addClass("active");
    $(`.main-security__step.step--${number}`).addClass("active");
  });

  $(".main-security__trigger-item").on("mouseleave", () => {
    $(".main-security__trigger-item").removeClass("active");
    $(`.main-security__step`).removeClass("active");
  });

  $(window).on("scroll load", () => {
    if ($(window).scrollTop() > 0) {
      $(".header").addClass("fixed");
    } else {
      $(".header").removeClass("fixed");
    }
  });

  $(".acardion__title").on("click", function openAcordion() {
    const thisItemParent = $(this).parent();
    if (thisItemParent.hasClass("active")) {
      thisItemParent.removeClass("active");
      thisItemParent.find(".acardion__inner").css("height", "0");
    } else {
      const itemHeight = thisItemParent
        .find(".acardion__content")
        .outerHeight();
      $(".acardion__item").removeClass("active");
      $(".acardion__inner").css("height", "0");
      $(".acardion-second__item").removeClass("active");
      $(".acardion-second__inner").css("height", "0");
      thisItemParent.addClass("active");
      thisItemParent.find(".acardion__inner").css("height", itemHeight);
    }
  });

  $(".show-password").on("click", function tooglePassword(e) {
    e.stopImmediatePropagation();
    const input = $(this).siblings("input");
    if ($(this).hasClass("active")) {
      input.attr("type", "password");
      $(this).removeClass("active");
    } else {
      input.attr("type", "text");
      $(this).addClass("active");
    }
  });

  $(".language__current").on("click", function openCloseLang() {
    $(this).toggleClass("active");
    $(".language__list").toggleClass("open");
  });

  $(document).on("click tachstart", e => {
    if (!$(e.target).closest(".language__current, .language__list").length) {
      $(".language__current").removeClass("active");
      $(".language__list").removeClass("open");
    }
    e.stopPropagation();
  });

  $(".menu-opener").on("click", () => {
    $(".main-menu").toggleClass("open");
  });

  $(".main-menu__close").on("click", () => {
    $(".main-menu").removeClass("open");
  });

  $(document).on("click tachstart", e => {
    if (!$(e.target).closest(".menu-opener, .main-menu").length) {
      $(".main-menu").removeClass("open");
    }
    e.stopPropagation();
  });

  function addedToMenu() {
    const traffisBtn = $(".tariffs-button");
    const languages = $(".language");
    const callback = $(".feedback-button");
    if (
      window.innerWidth <= 767 &&
      !$(".main-menu").find(".tariffs-button").length &&
      !$(".main-menu").find(".language").length &&
      !$(".main-menu").find(".feedback-button").length
    ) {
      $(".main-menu").prepend(traffisBtn);
      $(".main-menu").append(languages);
      $(".main-menu").prepend(callback);
    } else if (
      window.innerWidth > 767 &&
      $(".main-menu .tariffs-button") &&
      $(".main-menu .language") &&
      $(".main-menu .feedback-button")
    ) {
      $(".main-menu")
        .find(".tariffs-button")
        .remove();
      $(".main-menu")
        .find(".language")
        .remove();
      $(".main-menu")
        .find(".feedback-button")
        .remove();
    }
  }
  $(window).on("load resize", () => addedToMenu());

  $(".scroll-button").on("click", function ancordsLinks(e) {
    const id = $(this).attr("href");
    e.preventDefault();
    const top = $(id).offset().top - 100;
    $("body,html").animate({ scrollTop: top }, 1000);
  });
};
