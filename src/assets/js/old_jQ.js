import $ from "jquery";
import Clipboard from "clipboard";

export default () => {
  $.copyLink = new Clipboard(".copy-link");

  $(".wrapper").on("scroll load", () => {
    if ($(".wrapper").scrollTop() > 0) {
      $(".header").addClass("fixed");
    } else {
      $(".header").removeClass("fixed");
    }
  });

  $(".menu-opener").on("click", () => {
    $(".main-menu").toggleClass("open");
  });

  $(".main-menu__close").on("click", () => {
    $(".main-menu").removeClass("open");
  });

  $(document).on("click tachstart", e => {
    if (!$(e.target).closest(".menu-opener, .main-menu").length) {
      $(".main-menu").removeClass("open");
    }
    e.stopPropagation();
  });

  function addedToMenu() {
    const traffisBtn = $(".tariffs-button");
    const languages = $(".language");
    const callback = $(".feedback-button");
    if (
      window.innerWidth <= 767 &&
      !$(".main-menu").find(".tariffs-button").length &&
      !$(".main-menu").find(".language").length &&
      !$(".main-menu").find(".feedback-button").length
    ) {
      $(".main-menu").prepend(traffisBtn);
      $(".main-menu").append(languages);
      $(".main-menu").prepend(callback);
    } else if (
      window.innerWidth > 767 &&
      $(".main-menu .tariffs-button") &&
      $(".main-menu .language") &&
      $(".main-menu .feedback-button")
    ) {
      $(".main-menu")
        .find(".tariffs-button")
        .remove();
      $(".main-menu")
        .find(".language")
        .remove();
      $(".main-menu")
        .find(".feedback-button")
        .remove();
    }
  }
  $(window).on("load resize", () => addedToMenu());
};
