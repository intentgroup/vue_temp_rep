/* eslint-disable */
import $ from "jquery";
import "slick-carousel";
import ProgressBar from "progressbar.js";

export default () => {
  const mainSlidersOptions = {
    slidesToShow: 1,
    speed: 1000,
    fade: true,
    autoplay: false,
    rows: 0
  };

  const $slick = $(".main-section__lg-slider");

  $slick.slick({
    ...mainSlidersOptions,
    arrows: false,
    dots: true,
    autoplay: true,
    customPaging(slider, i) {
      return `<div class="circle" id="circle-${i}"></div><div class="dot"></div>`;
    },
    asNavFor: ".main-section__sm-slider"
  });

  const time = 3;
  const slides = $slick.slick("getSlick").slideCount;
  let isPause = false;
  let tick;
  let percentTime;
  let circles = [];

  const $bar = $(".slider-progress .progress");

  function interval() {
    if (isPause === false) {
      percentTime += 1 / (time + 0.1);
      $bar.css({
        width: `${percentTime}%`
      });
      if (percentTime >= 100) {
        $slick.slick("slickNext");
        startProgressbar();
      }
      const current = $slick.slick("slickCurrentSlide");
      circles[current].animate(percentTime / 100, {
        duration: 1
      });
    }
  }

  function resetProgressbar() {
    $bar.css({ width: `${0}%` });
    clearTimeout(tick);
  }

  function startProgressbar() {
    resetProgressbar();
    percentTime = 0;
    isPause = false;
    tick = setInterval(interval, 10);
  }

  if ($(".main-section__lg-slider .slick-dots").length) {
    for (let i = 0; i < slides; i += 1) {
      const circle = new ProgressBar.Circle(`#circle-${i}`, {
        color: "#fd864b",
        strokeWidth: 5
      });
      if ($(".main-section__lg-slider .slick-dots").length) {
        circles.push(circle);
      }
    }

    $slick.on({
      mouseenter() {
        isPause = true;
      },
      mouseleave() {
        isPause = false;
      }
    });

    if ($slick.length > 0) {
      startProgressbar();

      $(".slick-dots .dot").on("click", () => {
        console.log("click");
        startProgressbar();
      });
    }
  }

  function renderCounter(currentSlide, processStatus, slick) {
    const i = (currentSlide || 0) + 1;
    processStatus.html(
      `<span class="slide-current"> 0${i}</span>
       <span class="slide-all">0${slick.slideCount} </span>`
    );
  }

  $(".main-section__sm-slider").each(function eachMainSmallSlider() {
    const processStatus = $(this)
      .siblings(".main-section__navigation")
      .find(".main-section__counter");
    $(this)
      .on("init reInit afterChange", (event, slick, currentSlide, nextSlide) =>
        renderCounter(currentSlide, processStatus, slick)
      )
      .slick({
        ...mainSlidersOptions,
        arrows: true,
        dots: false,
        prevArrow: $(this)
          .siblings(".main-section__navigation")
          .find(".prev-arrow")
          .html(
            '<svg xmlns="http://www.w3.org/2000/svg" width="21.75" height="40.937" viewBox="0 0 21.75 40.937"><path d="M198.656,500.682l18.569-18.5a1.151,1.151,0,0,0,0-1.639,1.163,1.163,0,0,0-1.646,0l-19.4,19.322a1.152,1.152,0,0,0,0,1.64l19.4,19.314a1.174,1.174,0,0,0,.819.343,1.139,1.139,0,0,0,.818-0.343,1.152,1.152,0,0,0,0-1.64Z" transform="translate(-195.844 -480.219)"/></svg>'
          ),
        nextArrow: $(this)
          .siblings(".main-section__navigation")
          .find(".next-arrow")
          .html(
            '<svg xmlns="http://www.w3.org/2000/svg" width="21.75" height="40.937" viewBox="0 0 21.75 40.937"><path d="M1721.34,500.682l-18.57-18.5a1.163,1.163,0,1,1,1.65-1.639l19.4,19.322a1.165,1.165,0,0,1,0,1.64l-19.4,19.314a1.16,1.16,0,1,1-1.64-1.64Z" transform="translate(-1702.44 -480.219)"/></svg>'
          ),
        asNavFor: ".main-section__lg-slider"
      });
  });

  $(".main-about__lg-slider").slick({
    ...mainSlidersOptions,
    arrows: false,
    dots: false,
    rows: 0,
    asNavFor: ".main-about__sm-slider",
    responsive: [
      {
        breakpoint: 1070,
        settings: {
          adaptiveHeight: true
        }
      }
    ]
  });

  $(".main-about__sm-slider").each(function eachMainAboutSmallSlider() {
    const processStatus = $(this)
      .siblings(".main-section__navigation")
      .find(".main-section__counter");
    $(this)
      .on("init reInit afterChange", (event, slick, currentSlide, nextSlide) =>
        renderCounter(currentSlide, processStatus, slick)
      )
      .slick({
        slidesToShow: 2,
        focusOnSelect: true,
        speed: 1000,
        arrows: true,
        dots: false,
        rows: 0,
        variableWidth: true,
        prevArrow: $(this)
          .siblings(".main-section__navigation")
          .find(".prev-arrow")
          .html(
            '<svg xmlns="http://www.w3.org/2000/svg" width="21.75" height="40.937" viewBox="0 0 21.75 40.937"><path d="M198.656,500.682l18.569-18.5a1.151,1.151,0,0,0,0-1.639,1.163,1.163,0,0,0-1.646,0l-19.4,19.322a1.152,1.152,0,0,0,0,1.64l19.4,19.314a1.174,1.174,0,0,0,.819.343,1.139,1.139,0,0,0,.818-0.343,1.152,1.152,0,0,0,0-1.64Z" transform="translate(-195.844 -480.219)"/></svg>'
          ),
        nextArrow: $(this)
          .siblings(".main-section__navigation")
          .find(".next-arrow")
          .html(
            '<svg xmlns="http://www.w3.org/2000/svg" width="21.75" height="40.937" viewBox="0 0 21.75 40.937"><path d="M1721.34,500.682l-18.57-18.5a1.163,1.163,0,1,1,1.65-1.639l19.4,19.322a1.165,1.165,0,0,1,0,1.64l-19.4,19.314a1.16,1.16,0,1,1-1.64-1.64Z" transform="translate(-1702.44 -480.219)"/></svg>'
          ),
        asNavFor: ".main-about__lg-slider"
      });
  });

  function updateSliderState() {
    if (window.innerWidth > 1024) {
      $(".tariff-block__list").each(function eachSlider() {
        if ($(this).hasClass("slick-initialized")) {
          $(this).slick("unslick");
        }
      });
    } else {
      $(".tariff-block__list").each(function eachSlider() {
        if (!$(this).hasClass("slick-initialized")) {
          $(this).slick({
            slidesToShow: 3,
            speed: 1000,
            arrows: false,
            dots: true,
            infinite: false,
            rows: 0,
            responsive: [
              {
                breakpoint: 768,
                settings: {
                  slidesToShow: 2
                }
              },
              {
                breakpoint: 480,
                settings: {
                  slidesToShow: 1
                }
              }
            ]
          });
        }
      });
    }
  }
  updateSliderState();

  $(window).on("resize", updateSliderState);

  $(".article__lg-slider").slick({
    ...mainSlidersOptions,
    arrows: false,
    dots: false,
    rows: 0,
    asNavFor: ".article__sm-slider"
  });

  $(".article__sm-slider").each(function eachMainAboutSmallSlider() {
    const processStatus = $(this)
      .siblings(".main-section__navigation")
      .find(".main-section__counter");
    $(this)
      .on("init reInit afterChange", (event, slick, currentSlide, nextSlide) =>
        renderCounter(currentSlide, processStatus, slick)
      )
      .slick({
        slidesToShow: 4,
        focusOnSelect: true,
        speed: 1000,
        arrows: true,
        dots: false,
        rows: 0,
        variableWidth: true,
        prevArrow: $(this)
          .siblings(".main-section__navigation")
          .find(".prev-arrow")
          .html(
            '<svg xmlns="http://www.w3.org/2000/svg" width="21.75" height="40.937" viewBox="0 0 21.75 40.937"><path d="M198.656,500.682l18.569-18.5a1.151,1.151,0,0,0,0-1.639,1.163,1.163,0,0,0-1.646,0l-19.4,19.322a1.152,1.152,0,0,0,0,1.64l19.4,19.314a1.174,1.174,0,0,0,.819.343,1.139,1.139,0,0,0,.818-0.343,1.152,1.152,0,0,0,0-1.64Z" transform="translate(-195.844 -480.219)"/></svg>'
          ),
        nextArrow: $(this)
          .siblings(".main-section__navigation")
          .find(".next-arrow")
          .html(
            '<svg xmlns="http://www.w3.org/2000/svg" width="21.75" height="40.937" viewBox="0 0 21.75 40.937"><path d="M1721.34,500.682l-18.57-18.5a1.163,1.163,0,1,1,1.65-1.639l19.4,19.322a1.165,1.165,0,0,1,0,1.64l-19.4,19.314a1.16,1.16,0,1,1-1.64-1.64Z" transform="translate(-1702.44 -480.219)"/></svg>'
          ),
        asNavFor: ".article__lg-slider"
      });
  });
};
