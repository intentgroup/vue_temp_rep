function showPass(context, ref) {
  let el = context.$refs[ref];
  el.type === "password"
    ? el.setAttribute("type", "text")
    : el.setAttribute("type", "password");
}

const MAX_EVENT_PAGE_SIZE = 10;

export { showPass, MAX_EVENT_PAGE_SIZE };
