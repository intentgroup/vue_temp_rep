import { Auth } from "aws-amplify";
import { AmplifyEventBus } from "aws-amplify-vue";
import store from "../store";

function getUser() {
  return Auth.currentAuthenticatedUser()
    .then(user => {
      if (user && user.signInUserSession) {
        store.dispatch("User/setUser", user);
        if (
          localStorage.getItem("username") === null &&
          localStorage.getItem("user-email") === null &&
          localStorage.getItem("user-type") === null
        ) {
          localStorage.setItem("username", user.attributes.name);
          localStorage.setItem("user-email", user.attributes.email);
          localStorage.setItem(
            "user-type",
            user.attributes["custom:user_type"]
          );
        }
        return user;
      } else {
        return null;
      }
    })
    .catch(err => {
      console.log(err);
      return null;
    });
}

async function changePassword(oldPassword, newPassword) {
  const user = await Auth.currentAuthenticatedUser();
  let data = await Auth.changePassword(user, oldPassword, newPassword);
  AmplifyEventBus.$emit("authState", "changePassword");
  return data;
}

function signUp(username, password, email, type, ref, tariff, duration) {
  const attributes = {
    name: username,
    email,
    "custom:user_type": type
  };

  if (ref) attributes["custom:referrer_code"] = ref;
  if (tariff) attributes["custom:tariff"] = tariff;
  if (duration) attributes["custom:tafiff_duration"] = duration;

  return Auth.signUp({
    username,
    password,
    attributes
  }).then(data => {
    console.log(data);
    AmplifyEventBus.$emit("localUser", data.user);
    if (data.userConfirmed === false) {
      AmplifyEventBus.$emit("authState", "confirmSignUp");
    } else {
      AmplifyEventBus.$emit("authState", "signIn");
    }
    return data;
  });
}

function confirmSignUp(username, code) {
  return Auth.confirmSignUp(username, code)
    .then(data => {
      AmplifyEventBus.$emit("authState", "signIn");
      return data; // 'SUCCESS'
    })
    .catch(err => {
      console.log(err);
      throw err;
    });
}

function resendSignUp(username) {
  return Auth.resendSignUp(username)
    .then(() => {
      return "SUCCESS";
    })
    .catch(err => {
      console.log(err);
      return err;
    });
}

async function signIn(username, password) {
  const user = await Auth.signIn(username, password);
  if (user) {
    AmplifyEventBus.$emit("authState", "signedIn");
  }
}

function signOut() {
  return Auth.signOut()
    .then(data => {
      AmplifyEventBus.$emit("authState", "signedOut");
      localStorage.removeItem("username");
      localStorage.removeItem("user-type");
      localStorage.removeItem("user-email");
      return data;
    })
    .catch(err => {
      console.log(err);
      return err;
    });
}

async function forgotPassword(username) {
  await Auth.forgotPassword(username).then(data => {
    AmplifyEventBus.$emit("authState", "forgotPassword");
    return data;
  });
}

async function forgotPasswordSubmit(username, code, new_password) {
  await Auth.forgotPasswordSubmit(username, code, new_password).then(data => {
    AmplifyEventBus.$emit("authState", "forgotPasswordSubmit");
    return data;
  });
}

async function changeEmailAndName(user, name, email) {
  return await Auth.updateUserAttributes(user, { email, name }).then(data => {
    AmplifyEventBus.$emit("authState", "changeEmailAndName");
    return data;
  });
}

async function verifyCurrentUserAttributeSubmit(code) {
  return await Auth.verifyCurrentUserAttributeSubmit("email", code).then(
    data => {
      console.log(data);
      return data;
    }
  );
}

async function userAttributes(user) {
  return await Auth.userAttributes(user).then(data => {
    return data;
  });
}

export {
  getUser,
  signIn,
  signUp,
  confirmSignUp,
  resendSignUp,
  signOut,
  forgotPassword,
  forgotPasswordSubmit,
  changePassword,
  changeEmailAndName,
  userAttributes,
  verifyCurrentUserAttributeSubmit
};
