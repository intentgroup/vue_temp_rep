import Vue from "vue";
import App from "./App.vue";
import "vue-select/dist/vue-select.css";
import infiniteScroll from "vue-infinite-scroll";
import VueTheMask from "vue-the-mask";
import VueApollo from "vue-apollo";
import ApolloClient from "apollo-boost";
import { InMemoryCache } from "apollo-cache-inmemory";
import VueRouter from "vue-router";
import router from "./router";
import store from "./store";
import Amplify, * as AmplifyModules from "aws-amplify";
import { Auth } from "aws-amplify";
import { AmplifyPlugin } from "aws-amplify-vue";
import aws_exports from "./aws-exports";
import "./assets/scss/index.scss";
import old_jQ from "./assets/js/old_jQ";
import i18n from "./i18n";

Amplify.configure(aws_exports);

Amplify.configure({
  API: {
    graphql_headers: async () => {
      const session = await Auth.currentSession();
      return {
        Authorization: session.getIdToken().getJwtToken()
      };
    }
  }
});

Vue.use(AmplifyPlugin, AmplifyModules);
Vue.use(VueRouter, VueApollo);
Vue.use(VueTheMask);

Vue.use(infiniteScroll, { distance: 10 });

Vue.config.productionTip = false;

const cache = new InMemoryCache();

const apolloClient = new ApolloClient({
  cache,
  resolvers: {}
});

const apolloProvider = new VueApollo({ defaultClient: apolloClient });

Date.prototype.daysInMonth = function () {
  return 33 - new Date(this.getFullYear(), this.getMonth(), 33).getDate();
};

Vue.filter("ddmmyy", function (value) {
  if (!value) return "";
  const date_value = new Date(value);
  return `${date_value.getDate()}.${date_value.getMonth() + 1}.${date_value.getFullYear()}`;
});

export const bus = new Vue();

function fadeOut(el) {
  el.style.opacity = 1;
  (function fade() {
    if ((el.style.opacity -= 0.1) < 0) {
      el.style.display = "none";
    } else {
      requestAnimationFrame(fade);
    }
  })();
}

const feedbackButton = document.querySelector(".help-btn");

document.addEventListener("DOMContentLoaded", () => {
  new Vue({
    router,
    store,
    apolloProvider,
    i18n,
    render: h => h(App)
  }).$mount("#app");
  old_jQ();

  let preloader = document.querySelector(".preloader");
  if (preloader) {
    const preloaderTimeOut = setTimeout(() => {
      if (preloader) {
        fadeOut(preloader);
        clearTimeout(preloaderTimeOut);
      }
    }, 1000);
  }

  window.zESettings = {
    webWidget: {
      contactForm: {
        attachments: false
      },
      color: { theme: "#fc8249" }
    }
  };

  if (
    window.zE &&
    localStorage.getItem("username") &&
    localStorage.getItem("user-email")
  ) {
    window.zE("webWidget", "prefill", {
      name: {
        value: String(localStorage.getItem("username"))
      },
      email: {
        value: String(localStorage.getItem("user-email"))
      }
    });
  }
  if (feedbackButton) {
    feedbackButton.addEventListener("click", e => {
      e.preventDefault();
      // eslint-disable-next-line no-undef
      window.zE("webWidget", "toggl2e");
    });
  }
});
