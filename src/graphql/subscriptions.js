/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const updatedCustomer = /* GraphQL */ `
  subscription UpdatedCustomer($userId: ID!) {
    updatedCustomer(userId: $userId) {
      userId
      data {
        id
        email
        login
        plan {
          id
          title
          durationMonths
          isFree
        }
        registrationDate
        subscriptionExpirationDate
        connectionSettings {
          connected
          vpnBypassMACAddressesAllowed
          vpnBypassMACAddresses
          connectionType {
            title
          }
          serverLocation {
            id
            title
            percentAvailability
            countryCode
            address
          }
        }
        isPartner
        partnerAttributes {
          referralCode
          balance
          totalBonuses
          referrals {
            id
            email
            login
            registrationDate
            amount
          }
        }
      }
    }
  }
`;
export const updatedConnectionType = /* GraphQL */ `
  subscription UpdatedConnectionType($userId: ID!) {
    updatedConnectionType(userId: $userId) {
      userId
      data {
        title
      }
    }
  }
`;
export const updatedServerLocation = /* GraphQL */ `
  subscription UpdatedServerLocation($userId: ID!) {
    updatedServerLocation(userId: $userId) {
      userId
      data {
        id
        title
        percentAvailability
        countryCode
        address
      }
    }
  }
`;
export const updatedVpnBypassMacAddressesAllowed = /* GraphQL */ `
  subscription UpdatedVpnBypassMacAddressesAllowed($userId: ID!) {
    updatedVpnBypassMACAddressesAllowed(userId: $userId) {
      userId
      data
    }
  }
`;
export const createdCustomerEvent = /* GraphQL */ `
  subscription CreatedCustomerEvent($userId: ID!) {
    createdCustomerEvent(userId: $userId) {
      userId
      data {
        id
        timestamp
        isRead
        message
      }
    }
  }
`;
export const updatedCustomerEvents = /* GraphQL */ `
  subscription UpdatedCustomerEvents($userId: ID!) {
    updatedCustomerEvents(userId: $userId) {
      userId
      data {
        id
        timestamp
        isRead
        message
      }
    }
  }
`;
export const updatedRouter = /* GraphQL */ `
  subscription UpdatedRouter($userId: ID!) {
    updatedRouter(userId: $userId) {
      userId
      data {
        id
        name
        ipAddress
        portNumber
        login
        routerDetails {
          routerboard
          boardName
          model
          revision
          serialNumber
          firmwareType
          factoryFirmware
          currentFirmware
          upgradeFirmware
        }
        routerState {
          verification
          autoconfiguration
          errorDetails
        }
      }
    }
  }
`;
export const updatedConnectionState = /* GraphQL */ `
  subscription UpdatedConnectionState($userId: ID!) {
    updatedConnectionState(userId: $userId) {
      userId
      data
    }
  }
`;
export const createdVpnBypassMacAddress = /* GraphQL */ `
  subscription CreatedVpnBypassMacAddress($userId: ID!) {
    createdVPNBypassMACAddress(userId: $userId) {
      userId
      data
    }
  }
`;
export const updatedVpnBypassMacAddress = /* GraphQL */ `
  subscription UpdatedVpnBypassMacAddress($userId: ID!) {
    updatedVPNBypassMACAddress(userId: $userId) {
      userId
      data
    }
  }
`;
export const removedVpnBypassMacAddress = /* GraphQL */ `
  subscription RemovedVpnBypassMacAddress($userId: ID!) {
    removedVPNBypassMACAddress(userId: $userId) {
      userId
      data
    }
  }
`;
export const updatedRouterCommandStatus = /* GraphQL */ `
  subscription UpdatedRouterCommandStatus($userId: ID!) {
    updatedRouterCommandStatus(userId: $userId) {
      userId
      data {
        id
        completed
        success
        command
        commandOutput
      }
    }
  }
`;
