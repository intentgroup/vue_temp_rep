/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const changePlan = /* GraphQL */ `
  mutation ChangePlan($newPlanId: ID) {
    changePlan(newPlanId: $newPlanId) {
      userId
      data {
        id
        email
        login
        plan {
          id
          title
          durationMonths
          isFree
        }
        registrationDate
        subscriptionExpirationDate
        connectionSettings {
          connected
          vpnBypassMACAddressesAllowed
          vpnBypassMACAddresses
          connectionType {
            title
          }
          serverLocation {
            id
            title
            percentAvailability
            countryCode
            address
          }
        }
        isPartner
        partnerAttributes {
          referralCode
          balance
          totalBonuses
          referrals {
            id
            email
            login
            registrationDate
            amount
          }
        }
      }
    }
  }
`;
export const updateRouter = /* GraphQL */ `
  mutation UpdateRouter($routerInfo: RouterConfigInput) {
    updateRouter(routerInfo: $routerInfo) {
      userId
      data {
        id
        name
        ipAddress
        portNumber
        login
        routerDetails {
          routerboard
          boardName
          model
          revision
          serialNumber
          firmwareType
          factoryFirmware
          currentFirmware
          upgradeFirmware
        }
        routerState {
          verification
          autoconfiguration
          errorDetails
        }
      }
    }
  }
`;
export const uploadManualVerification = /* GraphQL */ `
  mutation UploadManualVerification(
    $routerId: ID!
    $verificationResults: String
  ) {
    uploadManualVerification(
      routerId: $routerId
      verificationResults: $verificationResults
    ) {
      userId
      data {
        id
        name
        ipAddress
        portNumber
        login
        routerDetails {
          routerboard
          boardName
          model
          revision
          serialNumber
          firmwareType
          factoryFirmware
          currentFirmware
          upgradeFirmware
        }
        routerState {
          verification
          autoconfiguration
          errorDetails
        }
      }
    }
  }
`;
export const markCustomerEventsRead = /* GraphQL */ `
  mutation MarkCustomerEventsRead($pageId: String, $messageIds: [ID]) {
    markCustomerEventsRead(pageId: $pageId, messageIds: $messageIds) {
      userId
      data {
        id
        timestamp
        isRead
        message
      }
    }
  }
`;
export const trackCustomerEvent = /* GraphQL */ `
  mutation TrackCustomerEvent($message: String) {
    trackCustomerEvent(message: $message) {
      userId
      data {
        id
        timestamp
        isRead
        message
      }
    }
  }
`;
export const createVpnBypassMacAddress = /* GraphQL */ `
  mutation CreateVpnBypassMacAddress($address: String) {
    createVPNBypassMACAddress(address: $address) {
      userId
      data
    }
  }
`;
export const updateVpnBypassMacAddress = /* GraphQL */ `
  mutation UpdateVpnBypassMacAddress($oldAddress: String, $newAddress: String) {
    updateVPNBypassMACAddress(
      oldAddress: $oldAddress
      newAddress: $newAddress
    ) {
      userId
      data
    }
  }
`;
export const removeVpnBypassMacAddress = /* GraphQL */ `
  mutation RemoveVpnBypassMacAddress($address: String) {
    removeVPNBypassMACAddress(address: $address) {
      userId
      data
    }
  }
`;
export const updateConnectionType = /* GraphQL */ `
  mutation UpdateConnectionType($newConnectionTypeTitle: String) {
    updateConnectionType(newConnectionTypeTitle: $newConnectionTypeTitle) {
      userId
      data {
        title
      }
    }
  }
`;
export const updateServerLocation = /* GraphQL */ `
  mutation UpdateServerLocation($newServerLocationId: ID!) {
    updateServerLocation(newServerLocationId: $newServerLocationId) {
      userId
      data {
        id
        title
        percentAvailability
        countryCode
        address
      }
    }
  }
`;
export const updateVpnBypassMacAddressesAllowed = /* GraphQL */ `
  mutation UpdateVpnBypassMacAddressesAllowed($newValue: Boolean) {
    updateVpnBypassMACAddressesAllowed(newValue: $newValue) {
      userId
      data
    }
  }
`;
export const verifyRouter = /* GraphQL */ `
  mutation VerifyRouter($routerId: ID!) {
    verifyRouter(routerId: $routerId) {
      userId
      data
    }
  }
`;
export const connectRouter = /* GraphQL */ `
  mutation ConnectRouter($routerId: ID!) {
    connectRouter(routerId: $routerId) {
      userId
      data
    }
  }
`;
export const disconnectRouter = /* GraphQL */ `
  mutation DisconnectRouter($routerId: ID!) {
    disconnectRouter(routerId: $routerId) {
      userId
      data
    }
  }
`;
export const applyRouterSettings = /* GraphQL */ `
  mutation ApplyRouterSettings($routerId: ID!) {
    applyRouterSettings(routerId: $routerId) {
      userId
      data
    }
  }
`;
export const applyRouterVpnBypassSettings = /* GraphQL */ `
  mutation ApplyRouterVpnBypassSettings($routerId: ID!) {
    applyRouterVPNBypassSettings(routerId: $routerId) {
      userId
      data
    }
  }
`;
export const forceDisconnectStatus = /* GraphQL */ `
  mutation ForceDisconnectStatus {
    forceDisconnectStatus {
      userId
      data
    }
  }
`;
export const updateConnectionState = /* GraphQL */ `
  mutation UpdateConnectionState($userId: ID!, $connected: Boolean) {
    updateConnectionState(userId: $userId, connected: $connected) {
      userId
      data
    }
  }
`;
export const updateRouterCommandStatus = /* GraphQL */ `
  mutation UpdateRouterCommandStatus($userId: ID!, $commandId: ID!) {
    updateRouterCommandStatus(userId: $userId, commandId: $commandId) {
      userId
      data {
        id
        completed
        success
        command
        commandOutput
      }
    }
  }
`;
