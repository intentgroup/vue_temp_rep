/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const customerInfo = /* GraphQL */ `
  query CustomerInfo {
    customerInfo {
      id
      email
      login
      plan {
        id
        title
        durationMonths
        isFree
      }
      registrationDate
      subscriptionExpirationDate
      connectionSettings {
        connected
        vpnBypassMACAddressesAllowed
        vpnBypassMACAddresses
        connectionType {
          title
        }
        serverLocation {
          id
          title
          percentAvailability
          countryCode
          address
        }
      }
      isPartner
      partnerAttributes {
        referralCode
        balance
        totalBonuses
        referrals {
          id
          email
          login
          registrationDate
          amount
        }
      }
    }
  }
`;
export const connectionTypes = /* GraphQL */ `
  query ConnectionTypes {
    connectionTypes {
      title
    }
  }
`;
export const serverLocations = /* GraphQL */ `
  query ServerLocations {
    serverLocations {
      id
      title
      percentAvailability
      countryCode
      address
    }
  }
`;
export const plans = /* GraphQL */ `
  query Plans {
    plans {
      id
      title
      durationMonths
      isFree
    }
  }
`;
export const getCustomerEventsPage = /* GraphQL */ `
  query GetCustomerEventsPage($previousPageId: String) {
    getCustomerEventsPage(previousPageId: $previousPageId) {
      id
      customerEvents {
        id
        timestamp
        isRead
        message
      }
    }
  }
`;
export const customerRouters = /* GraphQL */ `
  query CustomerRouters {
    customerRouters {
      id
      name
      ipAddress
      portNumber
      login
      routerDetails {
        routerboard
        boardName
        model
        revision
        serialNumber
        firmwareType
        factoryFirmware
        currentFirmware
        upgradeFirmware
      }
      routerState {
        verification
        autoconfiguration
        errorDetails
      }
    }
  }
`;
export const checkRouterCommandStatus = /* GraphQL */ `
  query CheckRouterCommandStatus($commandId: ID!) {
    checkRouterCommandStatus(commandId: $commandId) {
      id
      completed
      success
      command
      commandOutput
    }
  }
`;
export const manualConfiguration = /* GraphQL */ `
  query ManualConfiguration {
    manualConfiguration {
      script
      verifyScript
      privateKey
      certificate
    }
  }
`;
export const manualManipulation = /* GraphQL */ `
  query ManualManipulation {
    manualManipulation {
      connect
      disconnect
    }
  }
`;
export const manualVpnBypass = /* GraphQL */ `
  query ManualVpnBypass {
    manualVpnBypass
  }
`;
