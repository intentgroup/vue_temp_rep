module.exports = {
  pluginOptions: {
    i18n: {
      locale: "ru",
      fallbackLocale: "en",
      localeDir: "locales",
      enableInSFC: true
    }
  },
  pages: {
    index: {
      entry: "src/main.js",
      template: "public/app.html",
      filename: "app.html"
    },
    "sing-up": {
      entry: "src/main.js",
      template: "public/sign-up.html",
      filename: "sign-up.html",
      title: "sing-up"
    }
  }
};
